import React from 'react'
import './Header.css'

export const Header = ({isVisible, mode, setMode}) => {


  if (isVisible === true) {
    return (
    <div className="header">
      <h1 className="title">Todo list</h1>
    </div>)
  } else {
    return null;
  }
}
