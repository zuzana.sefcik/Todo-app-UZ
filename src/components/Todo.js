import React, { useState } from 'react';

import "./TodoList.css"

import Button from "@material-ui/core/Button";
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import dayJs from "dayjs";
import { grey, red } from '@material-ui/core/colors';
import { AirlineSeatFlatAngled } from '@material-ui/icons';

export const Todo = (props) => {

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [flag, setFlag] = React.useState(true);

  const handleCloseMenu = () => {
    setAnchorEl(null);
  }

  const handleEdit = () => {
    handleCloseMenu();
    props.onEdit(props.todo.id);
  }

  const handleRemove = () => {
    handleCloseMenu();
    props.onRemove(props.todo.id);
  }

  const setToFavorite = () => {
    setFlag(!flag);
  }
  
  return (
    <div className= {props.todo.completed ? "card completedCard" : "card"}>
    <Card>
      <CardHeader
      action={
          <IconButton aria-label="settings" onClick={ (event) => {setAnchorEl(event.currentTarget)}}>
            <MoreVertIcon />
          </IconButton>
        }
        title={props.todo.name}
        subheader={`Due on ${dayJs(props.todo.dueDate).format("D.M.YYYY")}`}
      />
      <Menu 
      id="simple-menu"
      anchorEl={anchorEl}
      keepMounted
      open={Boolean(anchorEl)}
      onClose={handleCloseMenu}
      >
        <MenuItem onClick={handleEdit}>Edit</MenuItem>
        <MenuItem onClick={handleRemove}>Remove</MenuItem>
      </Menu>
      <CardContent injectFirst>
        <div> 
          <p className="todoDescription">{props.todo.description}</p> 
          {props.todo.completed && <div><p className="done">DONE</p></div>}
        </div>
    </CardContent>
    <CardActions disableSpacing>
    <div className="buttons">
                {/* button done */}
                {!props.todo.completed 
                ? 
                <button className="button button-done" onClick={() => { return props.onComplete(props.todo.id)}}><i className="fas fa-check"></i></button>  
                :
                <button className="undoTask" onClick={ () => { return props.unCompleteTodo(props.todo.id)}}>UNDO</button>}
              </div>
          <IconButton 
          aria-label="add to favorites" 
          onClick={setToFavorite}
          style={flag ? {color: grey[500]} : {color: red[500]}}>
            <FavoriteIcon />
          </IconButton>
      </CardActions>
  </Card>
  </div>
  )
}