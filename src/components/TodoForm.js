import React, { useEffect, useState } from 'react'
import "./TodoForm.css";

export const TodoForm = (props) => {

  const [name, setName] = useState("");
  const [description, setDescription] = useState("")
  const [dueDate, setDueDate] = useState("");


  // ak sa zmeni props.todo tak spustit useEffect
  useEffect( () => {
    if (props.todo) {
      setName(props.todo.name);
      setDescription(props.todo.description);
      setDueDate(props.todo.dueDate);
    }
  }, [props.todo])

  const onFormSubmit = (e) => {
    e.preventDefault(); 

    if (props.todo === undefined) {
      props.onAdd({name, description, dueDate})
    } else {
      props.onUpdate({id: props.todo.id, name, description, dueDate})
    }
      setName('')
      setDescription('')
      setDueDate('');
  }

  return (
    <form onSubmit={onFormSubmit} >

        
        <input type="text" placeholder="Name your todo" value={name} 
        onChange={(e) => setName(e.target.value)} required/>

        
        <textarea placeholder="Describe your todo" value={description} 
        onChange={(e) => setDescription(e.target.value)}></textarea>

        <input type="date" value = {dueDate} onChange={ (e) => {setDueDate(e.target.value)}}/>

        {props.todo === undefined
        ? <button disabled={!name} className="button addButton"><i className="fas fa-plus fa-2x"></i></button>
        : <button className="button edit-button"><i className="fas fa-edit"></i></button>}
        
    </form>

    
  )
}