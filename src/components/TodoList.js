import React from 'react'

import './TodoList.css'

import { Todo } from './Todo'
import { Flipper, Flipped } from "react-flip-toolkit";
import { Flip } from '@material-ui/icons';

export const TodoList = (props) => {

  const statusHandler = (e) => {
    props.setStatus(e.target.value)
  }


  const todos = props.todos;
  if (todos.length === 0) {
    return <p>Nothing to do</p>
  } else {
    return (
      <div className="todo-list">
        <div className="todo-header">

          <p>Tasks left to do: {todos.filter( (todo) => todo.completed === false).length} </p>

          <select onChange={statusHandler} name="filterItems" id="filter">
            <option value="all">All</option>
            <option value="completed">Completed</option>
            <option value="uncompleted">Uncompleted</option>
          </select>
        </div>
        
        <Flipper flipKey={props.filteredTodos}>
            {props.filteredTodos.map((todo) => {
              return (
                <Flipped key={todo.id} flipId={todo.id}>
                  <div  className="todo-row">
                    
                    <Todo 
                    todo={todo}
                    onRemove={props.onRemove}
                    unCompleteTodo={props.unCompleteTodo}
                    onComplete={props.onComplete}
                    onEdit={props.onEdit}/>

                  </div>
                </Flipped>
              )
            })}
        </Flipper>
     </div>);
  }
}