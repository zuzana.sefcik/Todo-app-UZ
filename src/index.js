import React, { useState, useEffect, useCallback } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { Header } from './components/Header';
import { TodoList } from './components/TodoList';
import { TodoForm } from './components/TodoForm';

import './styles.css'


const App = () => {

  const [todos, setTodos] = useState([]);
  const [status, setStatus] = useState("all");
  const [filteredTodos, setFilteredTodos] = useState([]);
  const [editingId, setEditingId] = useState("");
  const [openDialog, setOpenDialog] = React.useState(false);
  

  useEffect( () => {
    filterHandler()
  }, [todos,status])


  const filterHandler = () => {
    switch(status) {
      case "completed":
        setFilteredTodos(todos.filter(todo => todo.completed === true));
        break;
      case "uncompleted":
        setFilteredTodos(todos.filter(todo => todo.completed === false));
        break;
      default:
        setFilteredTodos(todos);
        break;
    }
  }

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const loadTodos = useCallback(async () => {
    const response = await axios.get('http://localhost:3001/todos');
    setTodos(response.data)
  }, [])

  useEffect(() => {
   loadTodos(); 
  }, [loadTodos])
  

  const addTodo = async (todo) => {  
    const newTodo ={
      name: todo.name,
      description: todo.description,
      dueDate: todo.dueDate,
      completed: false
    }
    await axios.post('http://localhost:3001/todos', newTodo)
    loadTodos()
  }

  const removeTodo = async (id) => {
    await axios.delete(`http://localhost:3001/todos/${id}`)
    loadTodos();
  }

  const completeTodo = async (id) => {
    await axios.put(`http://localhost:3001/todos/${id}`, {completed: true})
    loadTodos();
  }

  const updateTodo = async (todo) => {
    console.log("updateTodo called with id " + todo.id);
    await axios.put(`http://localhost:3001/todos/${todo.id}`, todo)
    setEditingId("");
    loadTodos();
  }

  const unCompleteTodo = async (id) => {
    await axios.put(`http://localhost:3001/todos/${id}`, {completed: false})
    loadTodos();
  }
  
  const removeAllClick = async () => {
    await axios.delete('http://localhost:3001/todos/delete/all/')
    loadTodos();
    setOpenDialog(false);
  }

  const startEditing = (id) => {
    setEditingId(id);
  }

  const getMeToDoThatIEdit = () => {
    const todo = filteredTodos.find((todo) => todo.id === editingId)
    return todo;
  }

  return  (
    <div className="app">
      <div className="container">
        <Header isVisible={true} />
        
        <div className="content"> 
          <TodoForm 
          todo = {getMeToDoThatIEdit()}
          onAdd={addTodo}
          onUpdate={updateTodo}
          />
          
          <TodoList 
          todos={todos} 
          onRemove={removeTodo} 
          onComplete={completeTodo}
          setStatus={setStatus}
          unCompleteTodo={unCompleteTodo}
          filteredTodos={filteredTodos}
          onEdit={startEditing}
          /> 
          
          <div className="allButtons">  
            <button className="removeButton" onClick={ handleClickOpenDialog}>Remove All</button>
          </div>
        </div>
      </div>
      <Dialog
        open={openDialog}
        onClose={handleCloseDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Do you want to delete all Todo items?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Deleting all todo items cannot be reverted.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={removeAllClick} color="primary" autoFocus>
            Delete all
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);